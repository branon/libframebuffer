# libframebuffer

An X virtual framebuffer library for Simba

## What

libframebuffer is a library to automate the creation, management, and utilization of multiple X virtual display servers on a single host system using [`Xvfb`](https://www.x.org/archive/X11R7.6/doc/man/man1/Xvfb.1.xhtml). Broadly, it enables a scripter to:

1. Create fake display servers which possess a distinct set of input devices
2. Spawn arbitrary processes in the context of those display servers

This creates a scenario very similar to what [rdpwrap](https://github.com/stascorp/rdpwrap) does on Windows, especially as [utilized by the SRL community](https://villavu.com/forum/showthread.php?t=118173); but **without** hacks, dependencies on external programs, or misuse of network protocols. libframebuffer's routines are bound directly to common X utilities, meaning it is **only available to systems running X11**. This should mean most (all?) GNU/Linux distributions that are not using Wayland by default.

## Why

* **No hacks** 
  * Using Remote Desktop Protocol to create multiple concurrent Remote Desktop sessions on your local Windows machine is (in most cases) a fundamentally unsupported operation. rdpwrap is a clever hack which attempts to enable this behavior on versions of Windows that Microsoft has declared aren't allowed to have it. Certain updates to Windows are also known to [break rdpwrap](https://github.com/stascorp/rdpwrap/issues/194)

* **No hard dependencies on uncommon external programs** 
  * libframebuffer is pure X, using a standard set of tools that are guaranteed to be available anywhere X is installed 

* **No misuse of network protocols**
  * *Remote* Desktop Protocol is a networked protocol, intended to facilitate communication across a LAN or the Internet. The X11 protocol, while also networkable, is better-suited for use case that is limited to your local machine

* **Closer to cross-platform** 
  * libframebuffer will run anywhere X can run (it might even work in Cygwin!) while RDP is Windows-only. Free, cross-platform RDP implementations do happen to exist, see [FreeRDP](http://www.freerdp.com/)

* **Less hassle**
  * rdpwrap (at least, as far as I am aware) requires a nontrivial amount of user intervention to utilize. This means things like [creating distinct user accounts](https://villavu.com/forum/showthread.php?t=118173&p=1391327#post1391327) and to some extent initializing RDP sessions themselves may have to be done manually, at least until you can find a way to automate these things. libframebuffer is fully scriptable out of the box and requires no preconfiguration provided all required X11 utilities are already present on your system. rdpwrap also appears to be targeted specifically by updates to Windows that attempt to break its functionality: rdpwrap chases a moving target while the X11 protocol is quite stable and free of breakage by comparison


# How

Simply download `libframebuffer.simba` and include it in your script. Here is an example of some very basic usage. Documentation for each function can be found in the source file itself.

```
program fbtest;
{$i libframebuffer.simba}

var
  fb: TFramebuffer;

begin
  fb.dimensions := 1024x768; // specify some screen geometry
  
  fb.create(); // create a new buffer
  fb.run('/usr/bin/xterm'); // run a program
  fb.dump(); // dump an image of what's inside the buffer
end.  
```

Dependencies are covered in the source file(s). At a minimum, you will need to have X11 available in addition to the following utilities:

* Xvfb
* xwud (to use `dump()`)
* x11vnc/vncviewer (to use `vnc()`)
